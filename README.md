# README #

README ini berisi keterangan singkat mengenai script-script yang tersedia dan bagaimana cara menggunakannya.

### What is this repository for? ###

Repository ini berisi script cli untuk download data hadist dari [core.carihadis.com](http://core.carihadis.com)

### How do I get set up? ###

Hal yang diutamankan pada script yang tersedia disini adalah minimnya dependensi, sehingga memudahkan user untuk menjalankan aplikasi. Sementara ini hanya tersedia dalam Bash script dan tidak tertutup kemungkinan akan tersedia menggunakan solusi lainnya.

### Usages ###

Siapkan file berisi daftar hadis yang ingin didownload dalam format csv sebagai berikut:
```
No,KITAB,start,end
1,Akhlak_Rawi_Khatib,1,2
2,Al_Adabul_Mufrad,1,2
3,Arbain_Nawawi_I,1,4
4,Arbain_Nawawi_II,1,4
```

#### Bash: ####
```bash
./dl-hadist.sh nama_file_list.csv
```

