#!/bin/bash

OLDIFS=$IFS
export url=http://api.carihadis.com/
export MERGE_DIR="merged"
export CURL_OPTS=(--progress-bar --retry 10 --retry-delay 3 --retry-max-time 60 -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0")
# Arbain_Nawawi_I-merged

display_help() {
   echo "Error: No arguments supplied"
   echo "Usage: $0 <daftar_kitab>"
}

dl_hadist() {
   start_id=$2
   end_id=$3
   mkdir -p $1
   for i in $(eval echo "{$start_id..$end_id}")
   do
      echo "Downloading $1: $i of $3"
      curl "${CURL_OPTS[@]}" "$url?kitab=$1&id=$i" | jq '.data[]' > $1/$i.json
   done
   echo -ne "Merging json files into '$MERGE_DIR/$1-merge.json \t\t"
   merge_json $1
   echo " done."
}

merge_json() {
   jq -s '.' $1/*.json > $MERGE_DIR/$1-merged.json
}

main() {
   if [ $# -eq 0 ]
   then
      display_help
      exit 1
   fi
   
   [ ! -f $1 ] && { echo "$1 file not found"; exit 99; }
   mkdir -p merged
   while IFS="," read -r nom kitab start_id end_id 
   do 
      dl_hadist $kitab $start_id $end_id
   done < <(tail -n +2 $1)
   exit 0
}

main $1